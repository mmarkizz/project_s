<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('password_reset_tokens', function (Blueprint $table) {
            $table->string('secondname');
            $table->string('firstname');
            $table->string('lastname');
            $table->number('series');
            $table->number('number');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('Adress');
            $table->string('SNILS');
            $table->string('Card');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('password_reset_tokens');
    }
};
