<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\users;
use App\Models\YourModelName;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class MyPlaceController extends Controller
{
    public function showAvtorPage() {
        
        return view('Avtorization.avtor');
    }

    public function showHomePage(){
        
        return view('Home.home');
    }
    
    public function showRegistPage()
    {
        return view('Regisaration.regist');
    }



    public function register(Request $request)
    {
        $request->validate([
            'second-name' => 'required',
            'first-name' => 'required',
            'last-name' => 'required',
            'series' => 'required',
            'number' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            //'Adress' => 'required',
            //'SNILS' => 'required',
            //'Card' => 'required',

        ]);

        $user = new YourModelName();
        $user->secondname = $request->input('second-name');
        $user->firstname = $request->input('first-name');
        $user->lastname = $request->input('last-name');
        $user->series = $request->input('series');
        $user->number = $request->input('number');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        //$user->Adress = $request->input('Adress');
        //$user->SNILS = $request->input('SNILS');
        //$user->Card = $request->input('Card');
        $user->save();

        return redirect('/home')->with('status', 'You are now registered');
    }

    public function login(Request $request)
    {
        $username = $request->input('login');
        $enteredPassword = $request->input('password');

        $storedPassword = DB::table('users')->where('email', $username)->value('password');

        if ($storedPassword && password_verify($enteredPassword, $storedPassword)) {
            return redirect('/home');
        } else {
            echo "Ошибка авторизации";
        }
    }

    public function updateData(Request $request)
    {
        if ($request->has('updateData')) {
            $newSecondName = $request->input('newSecondName');
            $newFirstName = $request->input('newFirstName');
            $newLastName = $request->input('newLastName');
            $newSeriesPass = $request->input('newSeriesPass');
            $newNumberPass = $request->input('newNumberPass');
            $newEmail = $request->input('newEmail');
            $newPassword = $request->input('newPassword');
            // Получите остальные данные для обновления
            // Выполните запрос для обновления данных
            DB::table('users')->update([
                'secondname' => $newSecondName,
                'firstname' => $newFirstName,
                'lastname' => $newLastName,
                'series' => $newSeriesPass,
                'number' => $newNumberPass,
                'email' => $newEmail,
                'password' => Hash::make($newPassword),
                // обновление остальных полей
            ]);

            // Вывод сообщения об успешном обновлении
            return "Данные были успешно обновлены!";
        }
    }

    public function saveData(Request $request)
{
    if ($request->has('saveData')) {
        $field1 = $request->input('field1');
        $field2 = $request->input('field2');
        $field3 = $request->input('field3');
        // Выполните запрос для сохранения данных
        DB::table('users')->update([
            'Adress' => $field1,
            'SNILS' => $field2,
            'Card' => $field3,
            // сохранение остальных полей
        ]);

        // Вывод сообщения об успешном сохранении
        return "Поля успешно сохранены в базе данных!";
    }
}
};
