<?php
    include("database.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="index.css">
    <title>Document</title>
</head>
<body>
    <form action="index.php" method="post" class="form_reg">
        
        <h2>Регистрация</h2>
        Фамилия:<br>
        <input type="text" name="second-name"><br>
        Имя:<br>
        <input type="text" name="first-name"><br>
        Отчество:<br>
        <input type="text" name="last-name"><br>
        Серия паспорта:<br>
        <input type="number" name="series"><br>
        Номер паспорта:<br>
        <input type="number" name="number"><br>
        E-mail:<br>
        <input type="email" name="email"><br>
        password:<br>
        <input type="password" name="password"><br>
        
        <input type="submit" name="submit" value="register">
        <p>Уже есть аккаунт?</p><a href="avtorization.php" name="avto">Авторизуйтесь</a>
    </form>
</body>
</html>

<?php
    if($_SERVER["REQUEST_METHOD"]=="POST"){

        $secondname= filter_input(INPUT_POST, "second-name", FILTER_SANITIZE_SPECIAL_CHARS);
        $firstname= filter_input(INPUT_POST, "first-name", FILTER_SANITIZE_SPECIAL_CHARS);
        $lastname= filter_input(INPUT_POST, "last-name", FILTER_SANITIZE_SPECIAL_CHARS);
        $series= filter_input(INPUT_POST, "series", FILTER_SANITIZE_SPECIAL_CHARS);
        $number= filter_input(INPUT_POST, "number", FILTER_SANITIZE_SPECIAL_CHARS);
        $email= filter_input(INPUT_POST, "email", FILTER_SANITIZE_SPECIAL_CHARS);
        $password= filter_input(INPUT_POST, "password", FILTER_SANITIZE_SPECIAL_CHARS);

        if (empty($secondname)){
            echo"Please enter a username";
        }
        elseif(empty($firstname)){
            echo"Please enter a username";
        }
        elseif(empty($lastname)){
            echo"Please enter a username";
        }
        elseif(empty($series)){
            echo"Please enter a series passport";
        }
        elseif(empty($number)){
            echo"Please enter a number passport";
        }
        elseif(empty($email)){
            echo"Please enter a email";
        }
        elseif(empty($password)){
            echo"Please enter a password";
        }
        else{
            $hash= password_hash($password, PASSWORD_DEFAULT);
            $sql="INSERT INTO users (secondname, firstname, lastname, series, number, email, password)
                    VALUES('$secondname', '$firstname', '$lastname', '$series', '$number', '$email', '$hash')";
            mysqli_query($conn, $sql);
            echo "You are now registered";
        }
        if (isset($_POST["submit"])){
            header("Location: http://localhost/website/Home.php");
            exit();
        }
        if (isset($_POST["avto"])){
            header("Location: http://localhost/website/avtorization.php");
            exit();
        }
    }
    mysqli_close($conn);

?>