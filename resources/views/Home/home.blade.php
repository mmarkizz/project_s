<?php
    include("/Users/galun/myapp/resources/views/database.blade.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700;800&family=Montserrat:ital,wght@0,400;1,100&display=swap" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    <h1 class='avtor_form_h2'>Добро пожаловать в Личный кабинет!</h1>
    <h3 class='avtor_form_h2'>Здесь вы можете изменить свои данные:</h3>
    <form action="{{ route('updateData') }}" method="post" class='avtor_form'><!--Home.php-->
        @csrf
        <input type="text" name="newSecondName" placeholder="Введите новую фамилию"></br></br>
        <input type="text" name="newFirstName" placeholder="Введите новое имя"></br></br>
        <input type="text" name="newLastName" placeholder="Введите новую отчество"></br></br>
        <input type="number" name="newSeriesPass" placeholder="Введите новую серию"></br></br>
        <input type="number" name="newNumberPass" placeholder="Введите новый номер"></br></br>
        <input type="email" name="newEmail" placeholder="Введите новый Email"></br></br>
        <input type="password" name="newPassword" placeholder="Введите новый пароль"></br></br>
        <input type="submit" value="Обновить" name="updateData">
    </form><br><br><br>


    
    <h3 class='avtor_form_h2'>Здесь вы можете заполнить новые поля в базе данных:</h3>
    <form action="{{ route('saveData') }}" method="post" class='avtor_form'>
        @csrf
        Адрес: <br><br>
        <input type="text" name="field1"><br><br>
        СНИЛС:<br><br>
        <input type="text" name="field2"><br><br>
        Номер карты:<br><br>
        <input type="text" name="field3"><br><br>
        <input type="submit" value="Сохранить" name="saveData">
    </form>


    <?php/*
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["Update"])) {
            
            $newSecondName = $_POST["newSecondName"];
            $newFirstName = $_POST["newFirstName"];
            $newLastName = $_POST["newLastName"];
            $newSeriesPass = $_POST["newSeriesPass"];
            $newNumberPass = $_POST["newNumberPass"];
            $newEmail = $_POST["newEmail"];
            $newPassword = $_POST["newPassword"];
            $hash= password_hash($newPassword, PASSWORD_DEFAULT);

            $query = "UPDATE users SET secondname = '$newSecondName', firstname = '$newFirstName', lastname = '$newLastName', series = '$newSeriesPass', number = '$newNumberPass', email = '$newEmail', password = '$hash'";
            $result = mysqli_query($conn, $query);
            
            if ($result){
                echo "Данные были успешно обновлены!";
            }
             else {
                echo "Ошибка обновления данных";
            }
        }
    }
?>

<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["Save"])) {
            
            $field1 = mysqli_real_escape_string($conn, $_POST["field1"]);
            $field2 = mysqli_real_escape_string($conn, $_POST["field2"]);
            $field3 = mysqli_real_escape_string($conn, $_POST["field3"]);

            $query = "UPDATE users SET Adress='$field1', SNILS='$field2', Card='$field3'";
            $result = mysqli_query($conn, $query);
    
            if ($result) {
                echo "Поля успешно сохранены в базе данных!";
            } else {
                echo "Ошибка сохранения полей";
            };
        };
    };
mysqli_close($conn);

?>*/
</body>
</html>